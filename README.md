# Docker Images for HTML 5 Game Development

These are based on the [Node Debian images][] and add:

- Blender
- ImageMagick
- Make
- OptiPNG
- pngcrush

[Node Debian images]: https://hub.docker.com/_/node/


## Images

- registry.gitlab.com/joewreschnig/html5gamedev-docker:debian-9-node-8
- registry.gitlab.com/joewreschnig/html5gamedev-docker:debian-9-node-9
- registry.gitlab.com/joewreschnig/html5gamedev-docker:debian-9-node-10
